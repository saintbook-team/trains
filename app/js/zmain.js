$(function(){

	let wow = new WOW( { offset: 150} );
	wow.init();

	$('.hidden-menu').click(function(){
		$('menu').slideToggle()
	});

	let first = $('.first'),
		nav = $('nav')

	$(document).on('scroll', function(){
		if( $(this).scrollTop() >= first.innerHeight() - nav.innerHeight()){
			nav.css({
				'position': 'fixed',
				'left': 0,
				'right': 0,
				'top': 0
			})
		}else{
			nav.removeAttr('style')
		}
	});

	$('nav a').click(function(e){
		e.preventDefault();
		let scrollTo = $($(this).attr('data-id'));
		$('html, body').animate({
			'scrollTop': scrollTo.offset().top - nav.innerHeight()
		})
	});
})