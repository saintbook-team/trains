let gulp = require('gulp'),
concat = require('gulp-concat'),
mini = require('gulp-cssnano'),
browserSync = require('browser-sync');
sass = require('gulp-sass');

gulp.task('sass', () =>(
	gulp.src('app/sass/*')
	.pipe(sass())
	.pipe(concat('css/general.css'))
	.pipe(mini())
	.pipe(gulp.dest('dist'))
	.pipe(browserSync.reload({stream: true})) 
))
gulp.task('css', () =>(
	gulp.src('app/css/*')
	.pipe(mini())
	.pipe(gulp.dest('dist'))
))

gulp.task('html', () => (
	gulp.src('app/html/*')
	.pipe(concat('index.html'))
	.pipe(gulp.dest('dist'))
	.pipe(browserSync.reload({stream: true})) 
));
gulp.task('js', () => (
	gulp.src('app/js/*')
	.pipe(concat('main.js'))
	.pipe(gulp.dest('dist'))
	.pipe(browserSync.reload({stream: true})) 
));

gulp.task('img', () => (
	gulp.src('app/img/*')
	.pipe(gulp.dest('dist/img'))
	.pipe(browserSync.reload({stream: true})) 
));

gulp.task('browser-sync', function() { 
    browserSync({ 
        server: { 
            baseDir: 'dist'
        },
        notify: false 
    });
});

gulp.task('build', ['browser-sync','sass','css', 'html', 'js', 'img'],() =>{
	let fonts = gulp.src('app/fonts/*')
		.pipe(gulp.dest('dist/fonts'))
});

gulp.task('watch', ['build'],() => {
	gulp.watch('app/sass/*', ['sass'])
	gulp.watch('app/html/*', ['html'])
	gulp.watch('app/js/*', ['js'])
	gulp.watch('app/img/*', ['img'])
});

gulp.task('default', ['watch']);